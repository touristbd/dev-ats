﻿using System;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ATS.Model
{
    public class CustomFieldSet
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }


        public string Format { get; set; }
        public string Element { get; set; }
        public DateTime CreatedAt { get; set; }

        public DateTime UpdateddAt { get; set; }

        //public int UserId { get; set; }

        public string FieldValues { get; set; }

        public bool FieldEncrypted { get; set; }

        public string DbColumn { get; set; }
        public string HelpText { get; set; }


    }
}
