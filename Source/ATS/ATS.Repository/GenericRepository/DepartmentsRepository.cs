﻿using ATS.Context;
using ATS.Model;
using ATS.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATS.Repository.GenericRepository
{
    public class DepartmentsRepository : Repository<Departments>, IDepartments
    {
        public DepartmentsRepository(AtsDbContext context) : base(context)
        {
        }
    }
}
