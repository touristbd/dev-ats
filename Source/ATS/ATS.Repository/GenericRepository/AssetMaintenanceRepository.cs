﻿using System;
using System.Collections.Generic;
using System.Text;
using ATS.Context;
using ATS.Model;
using ATS.Repository.Interface;

namespace ATS.Repository.GenericRepository
{
    public class AssetMaintenanceRepository : Repository<AssetMaintenance>, IAssetMaintenance
    {
        public AssetMaintenanceRepository(AtsDbContext context) : base(context)
        {
        }
    }
}
