﻿using ATS.Context;
using ATS.Model;
using ATS.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATS.Repository.GenericRepository
{
    public class LicenseSeatRepository : Repository<LicenseSeat>, ILicenseSeat
    {
        public LicenseSeatRepository(AtsDbContext context) : base(context)
        {
        }
    }
}
