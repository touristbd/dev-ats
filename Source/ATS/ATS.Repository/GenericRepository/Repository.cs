﻿using ATS.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ATS.Repository.GenericRepository
{
    public class Repository<T> : IRepository<T> where T : class

    {
        private readonly AtsDbContext context;
        private DbSet<T> entities;
        string errorMessage = string.Empty;

        public Repository(AtsDbContext context)
        {
            this.context = context;
            entities = context.Set<T>();
        }

        public void Delete(T entity)
        {
            if(entity != null)
            {
                entities.Remove(entity);
                context.SaveChanges();
            }
        }



        public T Get(long id)
        {
            return entities.Find(id);
        }

        public IEnumerable<T> GetAll()
        {
            return entities.AsEnumerable();
        }

        public void Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entities.Add(entity);
            context.SaveChanges();
        }

        public void Update(T entity)
        {
            if(entity != null)
            {
                context.Entry<T>(entity).State = EntityState.Modified;
            }
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }

}
