﻿
using ATS.Context;
using ATS.Model;
using ATS.Repository.Interface;

namespace ATS.Repository.GenericRepository
{
    public class ComponentsRepository : Repository<Components>, IComponents
    {
        public ComponentsRepository(AtsDbContext context) : base(context)
        {
        }
    }
}
