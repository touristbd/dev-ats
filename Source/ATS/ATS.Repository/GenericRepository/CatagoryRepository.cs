﻿using ATS.Context;
using ATS.Model;
using ATS.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATS.Repository.GenericRepository
{
    public class CatagoryRepository : Repository<Category>, ICategory
    {
        public CatagoryRepository(AtsDbContext context) : base(context)
        {
        }
    }
}
