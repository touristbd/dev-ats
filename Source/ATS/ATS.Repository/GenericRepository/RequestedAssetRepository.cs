﻿using ATS.Context;
using ATS.Model;
using ATS.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATS.Repository.GenericRepository
{
    public class RequestedAssetRepository : Repository<RequestedAsset>, IRequestedAsset
    {
        public RequestedAssetRepository(AtsDbContext context) : base(context)
        {
        }
    }
}
