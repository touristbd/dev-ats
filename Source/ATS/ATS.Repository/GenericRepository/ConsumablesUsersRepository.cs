﻿using ATS.Context;
using ATS.Model;
using ATS.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATS.Repository.GenericRepository
{
    public class ConsumablesUsersRepository : Repository<ConsumablesUsers>, IConsumablesUsers
    {
        public ConsumablesUsersRepository(AtsDbContext context) : base(context)
        {
        }
    }
}
