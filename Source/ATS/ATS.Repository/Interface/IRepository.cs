﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ATS.Repository
{
    public interface IRepository<T> : IDisposable where T : class
    {
        IEnumerable<T> GetAll();
        T Get(long id);
        void Insert(T entity);
        void Update(T entity);
        void Delete(T entity);



    }
}
