﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATS.Model
{
    public class Import
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string FilePath { get; set; }

        public int FileSize { get; set; }

        public string ImportType { get; set; }
        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
        public string HeaderRow { get; set; }

        public string FirstRow { get; set; }

        public string FieldMap { get; set; }
    }
}
