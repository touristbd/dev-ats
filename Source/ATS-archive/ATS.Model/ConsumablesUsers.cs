﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATS.Model
{
    public class ConsumablesUsers
    {
        public int Id { get; set; }

        [ForeignKey("UserId")]
        public int UserId { get; set; }
        //public virtual User User { get; set; }

        [ForeignKey("ConsumableId")]
        public int ConsumableId { get; set; }
        public virtual Consumable Consumable { get; set; }

        [ForeignKey("AssignTo")]
        public int AssignTo { get; set; }
        //public virtual User User { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
