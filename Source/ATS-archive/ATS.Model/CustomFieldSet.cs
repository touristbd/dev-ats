﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATS.Model
{
    public class CustomFieldSet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Format { get; set; }
        public string Element { get; set; }
        public DateTime CreatedAt { get; set; }

        public DateTime UpdateddAt { get; set; }

        //public int UserId { get; set; }

        public string FieldValues { get; set; }

        public bool FieldEncrypted { get; set; }

        public string DbColumn { get; set; }
        public string HelpText { get; set; }


    }
}
