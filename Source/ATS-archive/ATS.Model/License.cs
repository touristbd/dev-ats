﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATS.Model
{
    public class License
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Serial { get; set; }

        public DateTime PurchaseDate { get; set; }

        public decimal PurchaseCost { get; set; }
        public string OrderNumber { get; set; }

        public int Seats { get; set; }
        public string Notes { get; set; }

        [ForeignKey("UserId")]
        public int UserId { get; set; }
        //public virtual User User { get; set; }

        [ForeignKey("DepreciationId")]
        public int DepreciationId { get; set; }
        public virtual Depreciation Depreciation { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public DateTime DeletedAt { get; set; }

        public string LicenseName { get; set; }
        public string LicenseEmail { get; set; }

        public bool Depreciate { get; set; }

        [ForeignKey("SupplierId")]
        public int SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }

        public DateTime ExpirationDate { get; set; }

        public string PurchaseOrder { get; set; }

        public DateTime TerminationDate { get; set; }

        public bool Maintained { get; set; }

        public bool Reassignable { get; set; }

        [ForeignKey("CompanyId")]
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }

        [ForeignKey("ManufacturerId")]
        public int ManufacturerId { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }
    }
}
