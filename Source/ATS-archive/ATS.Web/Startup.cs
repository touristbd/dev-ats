﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ATS.Web.Startup))]
namespace ATS.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
